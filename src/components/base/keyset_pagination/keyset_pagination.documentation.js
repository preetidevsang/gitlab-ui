import * as description from './keyset_pagination.md';

export default {
  description,
  followsDesignSystem: true,
};
